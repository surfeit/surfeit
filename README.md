# Surfeit

## What is Surfeit?
Surfeit is a GTD environment written in Python. Initially, this was just a set of utilities I wrote for myself, but now I'm expanding it into a more general set of utilities.

## Dependencies
Since this executes several shell scripts, `zathura` and `pdflatex` are required. If you want to swap these out for equivalent programs, edit the source code in `main.py`.

## Installation
Since Python isn't a compiled language, and Surfeit isn't on PyPI, installation requires `pipenv`, so you should run `pip install --user pipenv && pipenv install --system`. After installation, `surfeit` can be run from `src/main.py` as its entry point using e.g. `python3 src/main.py edit`.
## Usage

Video tutorial to come!
